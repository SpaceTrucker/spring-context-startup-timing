import java.io.IOException;
import java.io.PrintWriter;

import org.junit.Test;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.support.AbstractBeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.gitlab.spacetrucker.springcontextstartuptiming.BeanInitializationTiming;

public class InitializationTimingTest {

	@Test
	public void foo() throws BeansException, IOException {
		try (ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("/test-context.xml")) {
			AbstractBeanFactory beanFactory = (AbstractBeanFactory) context.getBeanFactory();
			System.out.println(beanFactory.getBeanPostProcessors());
			try (PrintWriter writer = new PrintWriter(System.out)) {
				context.getBean(BeanInitializationTiming.class).writeReportTo(writer);
			}
		}
	}

}
