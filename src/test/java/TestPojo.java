import java.math.BigDecimal;

public class TestPojo {

	private String foo;
	private BigDecimal bar;
	private Object blub;

	public void setBar(BigDecimal bar) throws InterruptedException {
		Thread.sleep(5);
		this.bar = bar;
	}

	public void setFoo(String foo) throws InterruptedException {
		Thread.sleep(25);
		this.foo = foo;
	}

	public void setBlub(Object blub) throws InterruptedException {
		Thread.sleep(3);
		this.blub = blub;
	}
}
