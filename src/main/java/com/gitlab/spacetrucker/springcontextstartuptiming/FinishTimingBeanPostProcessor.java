package com.gitlab.spacetrucker.springcontextstartuptiming;

import javax.annotation.Resource;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.core.Ordered;

public class FinishTimingBeanPostProcessor implements BeanPostProcessor, Ordered {

	@Resource
	private BeanInitializationTiming beanInitializationTiming;

	private int order = Ordered.LOWEST_PRECEDENCE;

	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		return bean;
	}

	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		beanInitializationTiming.finishTiming(beanName);
		return bean;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	@Override
	public int getOrder() {
		return order;
	}

}
