package com.gitlab.spacetrucker.springcontextstartuptiming;

import java.io.IOException;
import java.io.Writer;
import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;

public class HierachicalBeanInitializationTiming implements BeanInitializationTiming {

	private static class Timing {
		private final List<Timing> nested = new ArrayList<>();
		private final String name;
		private final Instant start;
		private Instant end;

		public Timing(String name, Instant start) {
			super();
			this.name = name;
			this.start = start;
		}

		public long getDurationMillis() {
			return Duration.between(start, end).toMillis();
		}
	}

	private final Deque<Timing> timingStack = new ConcurrentLinkedDeque<>();
	private final Map<String, Timing> beanNameToTiming = new ConcurrentHashMap<>();

	private final Clock clock;

	public HierachicalBeanInitializationTiming() {
		clock = Clock.systemDefaultZone();
		timingStack.push(new Timing("<root timing>", null));
	}

	public HierachicalBeanInitializationTiming(Clock clock) {
		super();
		this.clock = clock;
		timingStack.push(new Timing("<root timing>", null));
	}

	@Override
	public void initializationStarted(String beanName) {
		Timing timing = new Timing(beanName, clock.instant());
		timingStack.peek().nested.add(timing);
		timingStack.push(timing);
		beanNameToTiming.put(beanName, timing);
	}

	@Override
	public void finishTiming(String beanName) {
		if (timingStack.size() < 1) {
			throw new IllegalStateException();
		}
		Timing timing = beanNameToTiming.get(beanName);
		timing.end = clock.instant();
		timingStack.pop();
	}

	@Override
	public void writeReportTo(Writer writer) throws IOException {
		Timing current = timingStack.peek();
		String intendation = "";
		writeTiming(writer, current, intendation);
	}

	private void writeTiming(Writer writer, Timing current, String intendation) throws IOException {
		if (current.end != null) {
			writer.write(intendation + format(current) + "\n");
		}
		String nextLevelIntendation = intendation + "    ";
		for (Timing nestedTiming : current.nested) {
			writeTiming(writer, nestedTiming, nextLevelIntendation);
		}
		if (current.end != null) {
			writer.write(nextLevelIntendation + formatSelfTime(current) + "\n");
		}
	}

	private String format(Timing timing) {
		return timing.name + " initialization " + timing.getDurationMillis() + "ms";
	}

	private String formatSelfTime(Timing timing) {
		long selfTime = timing.getDurationMillis() - calculateNestedTimingDuration(timing).toMillis();
		return timing.name + " self time " + selfTime + "ms";
	}

	private Duration calculateNestedTimingDuration(Timing timing) {
		return Duration.ofMillis(
				timing.nested.stream().filter(e -> e.end != null).mapToLong(e -> e.getDurationMillis()).sum());
	}

}
