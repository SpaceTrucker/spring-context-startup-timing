package com.gitlab.spacetrucker.springcontextstartuptiming;

import java.io.IOException;
import java.io.Writer;

public interface BeanInitializationTiming {

	void initializationStarted(String beanName);

	void finishTiming(String beanName);

	void writeReportTo(Writer writer) throws IOException;

}
